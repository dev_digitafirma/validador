# DigitaFirma Validador

An electronic signature mechanism is used to verify the authenticity of digital messages or documents. A valid digital signature, gives a recipient very strong reason to believe that the message was created by a known sender, and that the message was not altered in transit.

This application performs the validation of advanced electronic signatures that were made using the FIEL issued by the SAT Authority and also provides the funcionability to validate others certificate-based digital signatures like [PaDes](https://acrobat.adobe.com/us/en/sign/digital-signatures.html) or [Timestamp signatures](https://en.wikipedia.org/wiki/Trusted_timestamping).

You can validate the following scenarios:

- #### PKI SIGNATURE
Validates an electronic signature that was made with a private key. For example a digital signature made with OpenSSL or a programing language.


- #### NOM-151 
The [NOM-151](https://dof.gob.mx/nota_detalle.php?codigo=5478024&fecha=30/03/2017) defines the requirements that must be observed for the conservation of data messages and digitization of documents in Mexico.  


- #### PAdES
ETSI/ESI Technical Standard (TS) 102 778, better known as PAdES (pronounced with either a long or short a), documents how the digital signature format described in ISO 32000-1 meets the needs of the 1999 EU Signature Directive (see previous blog entry), and then goes on to describe how that format can be expanded to take advantage of certain capabilities such as long-term document validation, where digital signatures placed on documents today can be validated five, ten and even 50 years later.  


- #### DigitaFirma XML File
DigitaFirma issues a XML file when a user sign a electronic document in the [platform](https://digitafirma.com). This file contains information about the signing process such as the signing method, the signers, original document and the NOM-151 constancy that protects the signed document. This is an integral/complete validation.  
    


## Structure
The project has a frontend component (Vue) and a backend (Java) component (maybe you may be thinking, seriously, front-end and backend for this small project? And maybe you're right but I found it funny to do it this way).

By this structure you can perform validations via the web portal or via the API.

### Folder structure

```shell
src
├── front
│   ├── public
│   └── src
├── main
│   ├── java
│   └── resources
└── test
    ├── java
    └── resources
```


## Getting Started 

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To install and run the project, you will need the following

```
- Java SDK 8
- Apache Maven 3.6.0
- NodeJS 12.xx.x
- npm 6.xx.x
```


### Installing

Clone this repository 

```bash
git clone https://gam_codes@bitbucket.org/dev_digitafirma/validador.git
```

Install de frontend dependencies

```bash
cd src/front
npm install
```

Before build the maven artifacts, we need to tell to the frontend where the backend is

```bash
vim src/front/src/axios.js
```
and set the `baseURL` variable to the backend location. **For now the backend only listens on the port 8080**

So, in a local enviroment, you should do this on axios.js file :

```javascript
// axios
import axios from 'axios'

const baseURL = 'http://localhost:8080/api'

```

## Running the tests

To run the tests
```bash
mvn test
```


## Deployment

To deploy the project locally, run the following:

```bash
# we must be in the root directory of the project

cd /path/to/validador

# artifacts

mvn clean install
```

Then, you will find the output jar in the `build` directory

```bash
# run the java application

java -jar build/validador-jar-with-dependencies.jar 
```

Finally you can see the validator on http://localhost:8080.

For more about the validation API, visit this [link](https://docs.digitafirma.com/)

## Built With

* [Javalin](https://javalin.io/) - The backend micro framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Vue](https://rometools.github.io/rome/) - Used to generate the frontend.
* [Vuesax](https://vuesax.com/) - Framework
Components for Vuejs
* [tailwindcss](https://tailwindcss.com/) - CSS framework


## Authors

* **DigitaFirma** - *Initial work* - [DigitaFirma](https://digitafirma.com)


## License

This project is licensed under the MIT License.