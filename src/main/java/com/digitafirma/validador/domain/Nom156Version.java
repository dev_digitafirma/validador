/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;

/**
 * @author gamboa on 6/6/20
 */
public enum Nom156Version {

    V1("SCFI-2012"),
    V2("SCFI-2016");

    private String value;

    private Nom156Version(String value)
    {
        this.value = value;
    }
    public String getName() {
        return value;
    }

}
