/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;



/**
 * @author gamboa on 5/24/20
 */
public enum DigestAlgorithm {

    MD5("MD5","1.2.840.113549.2.5"),
    SHA256("SHA256","2.16.840.1.101.3.4.2.1"),
    SHA512("SHA512","2.16.840.1.101.3.4.2.3"),
    SHA1("SHA1","1.3.14.3.2.26"),
    UNKNOWN("UNKNOWN","0.0.0.0.");

    private String name;

    private String oid;

    private DigestAlgorithm(String name, String oid)
    {
        this.name = name;
        this.oid = oid;
    }

    public static DigestAlgorithm valueOfOid(String oid){
        for (DigestAlgorithm e : values()) {
            if (e.getOid().equals(oid)) {
                return e;
            }
        }
        return UNKNOWN;
    }

    public String getName() {
        return name;
    }

    public String getOid() {
        return oid;
    }
}
