/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.constancy;

/**
 * @author gamboa on 5/31/20
 */
public class CertificateResult {

    // issuer
    private String issuer;

    private String serialNumber;
    //serialNumber

    private boolean validOnSignatureDate;

    private CertStatus certStatus;

    public CertificateResult(String issuer,
                             String serialNumber,
                             boolean validOnSignatureDate,
                             CertStatus certStatus
    )
    {
        this.issuer = issuer;
        this.serialNumber = serialNumber;
        this.validOnSignatureDate = validOnSignatureDate;
        this.certStatus = certStatus;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public boolean isValidOnSignatureDate() {
        return validOnSignatureDate;
    }

    public CertStatus getCertStatus() {
        return certStatus;
    }
}
