/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.constancy;

import com.digitafirma.validador.domain.ValidationResult;
import com.digitafirma.validador.domain.pdf.SignerResult;

import java.util.List;

/**
 * @author gamboa on 5/30/20
 */
public class TimeStampValidationResult extends ValidationResult {


    private String policy;
    private boolean imprintMessageValid;
    private List<SignerResult> signerResults;
    //private TsrStatusInfoResult tsrStatusResult;
    private String imprintMessageDigest;


    public TimeStampValidationResult(List<SignerResult> signerResults,
                                     String constancyPolicy,
                                     boolean impMsjStatus,
                                     String imprintMessageDigest) {
        super();
        this.signerResults = signerResults;
        this.policy = constancyPolicy;
        //this.tsrStatusResult = tsrStatusResult;
        this.imprintMessageValid = impMsjStatus;
        this.imprintMessageDigest = imprintMessageDigest;

    }

    public String getPolicy() {
        return policy;
    }

    public boolean isImprintMessageValid() {
        return imprintMessageValid;
    }

    public List<SignerResult> getSignerResults() {
        return signerResults;
    }

    /*public TsrStatusInfoResult getTsrStatusResult() {
        return tsrStatusResult;
    }*/

    public String getImprintMessageDigest() {
        return imprintMessageDigest;
    }
}
