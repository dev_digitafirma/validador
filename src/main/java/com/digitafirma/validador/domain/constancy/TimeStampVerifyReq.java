/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.constancy;

import com.digitafirma.validador.domain.DigitalDocument;
import com.digitafirma.validador.util.ASNUtil;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;

import java.io.IOException;

/**
 * @author gamboa on 5/30/20
 */
public class TimeStampVerifyReq {

    private TimeStampToken stampToken;
    private DigitalDocument document;

    /**
     *
     * @param stampToken the Timestamp token from the TSR
     * @param document
     */
    public TimeStampVerifyReq(TimeStampToken stampToken, DigitalDocument document) {
        this.stampToken = stampToken;
        this.document = document;
    }


    /**
     *
     * @param document
     * @param tsr the Timestamp response
     * @throws IOException
     * @throws TSPException
     */
    public TimeStampVerifyReq (DigitalDocument document, byte[] tsr) throws IOException, TSPException {
        this.document = document;
        this.stampToken = ASNUtil.buildTsrToken(tsr);
    }


    public TimeStampToken getStampToken() {
        return this.stampToken;
    }

    public DigitalDocument getDocument() {
        return this.document;
    }
}
