/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.constancy;

/**
 * @author gamboa on 6/21/20
 */
public enum CertStatus {

    VALID("Certificate valid",200),
    NOT_SIGN_TIME("No sign time provider",202),
    EXPIRED("Certificate expired",203),
    NOT_YET_VALID("Certificate not yet valid",204),
    SELF_SIGNED("Certificate self signed",205);

    public String msg;
    public int code;

    CertStatus(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }


}
