/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.constancy;

import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.Result;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

/**
 * @author gamboa on 5/30/20
 */
public interface TimeStampVerifyOperation {

    /**
     *
     * @param document The digital document as InputStream
     * @param documentName the Digital document's name
     * @param tsaResponse the base64 encoded TimeStamp Authority response (AKA ReachCore)
     * @return TimeStampValidationResult object
     */
    public Result execute(InputStream document, String documentName, String tsaResponse)
            throws IOException,
            TSPException,
            GeneralSecurityException,
            OperatorCreationException,
            InvalidCertCountException,
            CMSException, NoCertificateException;
}
