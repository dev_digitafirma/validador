/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;

/**
 * @author gamboa on 5/28/20
 */
public  class Routes {

   public static class  API{
        private static final String prefix = "/api";
        public static final String SIGN = prefix + "/validateSign";
        public static final String CONSTANCY = prefix + "/validateConstancy";
        public static final String XML = prefix + "/validateXML";
        public static final String PDF = prefix + "/validatePDF";
    }

    public static class View{
        public static final String SIGNATURE = "/signature";
    }
}
