/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */


package com.digitafirma.validador.domain;

import com.digitafirma.validador.domain.xml.adapter.ContentAdapter;
import org.apache.commons.io.IOUtils;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author gamboa on 5/24/20
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class DigitalDocument {

    @XmlAttribute(name = "name")
    private String name;

    @XmlJavaTypeAdapter(ContentAdapter.class)
    @XmlValue
    private byte[] content;

    @XmlAttribute(name = "contentType")
    private String contentType;


    public DigitalDocument(String name, byte[] content) {
        this.name = name;
        this.content = content;
    }

    public DigitalDocument(){}


    public String getName() {
        return name;
    }


    public byte[] getContent() {
        return content;
    }

    /*public String getB64Content() throws IOException {
        return Base64.getEncoder().encodeToString(IOUtils.toByteArray(content));
    }*/


    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

}
