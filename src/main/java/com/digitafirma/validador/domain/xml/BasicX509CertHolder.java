/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import com.digitafirma.validador.domain.xml.adapter.BigIntegerAdapter;
import com.digitafirma.validador.domain.xml.adapter.ContentAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigInteger;

/**
 * @author gamboa on 6/6/20
 */

@XmlRootElement(name = "signer")
@XmlAccessorType(XmlAccessType.FIELD)
public class BasicX509CertHolder {

    @XmlJavaTypeAdapter(BigIntegerAdapter.class)
    @XmlAttribute(name = "number")
    private BigInteger number;

    @XmlAttribute(name = "issuer")
    private String issuerName;

    @XmlJavaTypeAdapter(ContentAdapter.class)
    @XmlValue
    private byte[] content;

    public BasicX509CertHolder(){

    }


    public BigInteger getNumber() {
        return number;
    }

    public void setNumber(BigInteger number) {
        this.number = number;
    }


    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }


    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
