/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import javax.xml.bind.annotation.*;

/**
 * @author gamboa on 6/6/20
 */

@XmlRootElement(name = "signers")
@XmlAccessorType(XmlAccessType.FIELD)
public class Signer {

    @XmlAttribute(name = "rfc")
    private String rfc;

    @XmlAttribute(name = "curp")
    private String curp;

    @XmlAttribute(name = "name")
    private String name;

    @XmlAttribute(name = "email")
    private String email;

    @XmlElement(namespace = "https://digitafirma.com/validador", name = "certificate")
    private BasicX509CertHolder certificate;

    @XmlElement(namespace = "https://digitafirma.com/validador", name = "signature")
    private SignatureInfo signatureInfo;

    public Signer() {
    }


    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }


    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public BasicX509CertHolder getCertificate() {
        return certificate;
    }

    public void setCertificate(BasicX509CertHolder certificate) {
        this.certificate = certificate;
    }


    public SignatureInfo getSignatureInfo() {
        return signatureInfo;
    }

    public void setSignatureInfo(SignatureInfo signatureInfo) {
        this.signatureInfo = signatureInfo;
    }
}
