package com.digitafirma.validador.domain.xml;

import com.digitafirma.validador.domain.DigestAlgorithm;
import com.digitafirma.validador.domain.xml.adapter.AlgorithmAdapter;
import com.digitafirma.validador.domain.xml.adapter.ContentAdapter;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class SignatureContent {


    @XmlJavaTypeAdapter(AlgorithmAdapter.class)
    @XmlAttribute(name = "algorithm")
    private DigestAlgorithm algorithm;

    @XmlJavaTypeAdapter(ContentAdapter.class)
    @XmlValue
    private byte[] signatureContent;

    public SignatureContent() {

    }


    public DigestAlgorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(DigestAlgorithm algorithm) {
        this.algorithm = algorithm;
    }


    public byte[] getSignatureContent() {
        return signatureContent;
    }

    public void setSignatureContent(byte[] signatureContent) {
        this.signatureContent = signatureContent;
    }
}
