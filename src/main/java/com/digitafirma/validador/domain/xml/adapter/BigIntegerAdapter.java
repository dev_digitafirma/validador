/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigInteger;

/**
 * @author gamboa on 6/6/20
 */
public class BigIntegerAdapter extends XmlAdapter<String, BigInteger> {
    @Override
    public BigInteger unmarshal(String s) throws Exception {
        return new BigInteger(s);
    }

    @Override
    public String marshal(BigInteger bigInteger) throws Exception {
        return "0";
    }
}
