/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import com.digitafirma.validador.domain.DigitalDocument;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author gamboa on 6/6/20
 */

@XmlRootElement( name = "signedDocument")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSummary {


    private SdVersion version = SdVersion.V1;

    @XmlElement( name = "file")
    private DigitalDocument document;


    @XmlElementWrapper( name = "signers")
    @XmlElement( name = "signer")
    private List<Signer> signers;

    @XmlElement( name = "constancy")
    private Timestamp nom151;

    @XmlAttribute(name = "signed")
    private boolean signed;

    @XmlAttribute(name = "pdfSignature")
    private boolean isPdfSignature;


    public XmlSummary() { }

    public SdVersion getVersion() {
        return version;
    }

    public void setVersion(SdVersion version) {
        this.version = version;
    }


    public DigitalDocument getDocument() {
        return document;
    }

    public void setDocument(DigitalDocument document) {
        this.document = document;
    }


    public List<Signer> getSigners() {
        return signers;
    }

    public void setSigners(List<Signer> signers) {
        this.signers = signers;
    }


    public Timestamp getNom151() {
        return nom151;
    }

    public void setNom151(Timestamp nom151) {
        this.nom151 = nom151;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public boolean isPdfSignature() {
        return isPdfSignature;
    }

    public XmlSummary setPdfSignature(boolean pdfSignature) {
        isPdfSignature = pdfSignature;
        return this;
    }
}
