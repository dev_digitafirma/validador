/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import com.digitafirma.validador.domain.Nom156Version;
import com.digitafirma.validador.domain.xml.adapter.ContentAdapter;
import com.digitafirma.validador.domain.xml.adapter.LocalDateAdapter;
import com.digitafirma.validador.domain.xml.adapter.VersionAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

/**
 * @author gamboa on 6/6/20
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Timestamp {

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    @XmlAttribute(name = "datetime")
    private LocalDateTime dateTime;

    @XmlJavaTypeAdapter(VersionAdapter.class)
    @XmlAttribute(name = "version")
    private Nom156Version version;

    @XmlAttribute(name = "policy")
    private String policy;

    @XmlJavaTypeAdapter(ContentAdapter.class)
    @XmlElement(name = "asn1")
    private byte[] tsr;
    /*TimeStamp Response ASN1 coded in b64*/

    public Timestamp() {
    }


    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }


    public Nom156Version getVersion() {
        return version;
    }

    public void setVersion(Nom156Version version) {
        this.version = version;
    }


    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }


    public byte[] getTsr() {
        return tsr;
    }

    public void setTsr(byte[] tsr) {
        this.tsr = tsr;
    }
}
