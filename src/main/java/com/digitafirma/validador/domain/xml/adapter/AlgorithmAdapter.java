/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml.adapter;

import com.digitafirma.validador.domain.DigestAlgorithm;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author gamboa on 6/6/20
 */
public class AlgorithmAdapter extends XmlAdapter<String, DigestAlgorithm> {

    @Override
    public DigestAlgorithm unmarshal(String s) throws Exception {
        return DigestAlgorithm.valueOf(s);
    }

    @Override
    public String marshal(DigestAlgorithm digestAlgorithm) throws Exception {
        return "0";
    }
}
