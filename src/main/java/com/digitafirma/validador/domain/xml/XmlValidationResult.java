/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.pdf.SignedDocumentValidResult;

/**
 * @author gamboa on 6/6/20
 */
public class XmlValidationResult {

    private SignedDocumentValidResult documentValidResult;
    private TimeStampValidationResult tsrResult;
    private String signatureType;

    public XmlValidationResult(SignedDocumentValidResult pdfValidationResult,
                               TimeStampValidationResult tsrResult,
                               String signatureType)

    {
        this.documentValidResult = pdfValidationResult;
        this.tsrResult = tsrResult;
        this.signatureType = signatureType;
    }

    public SignedDocumentValidResult getDocumentValidResult() {
        return documentValidResult;
    }

    public TimeStampValidationResult getTsrResult() {
        return tsrResult;
    }

    public String getSignatureType() {
        return signatureType;
    }
}
