/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml;

import javax.xml.bind.annotation.*;

/**
 * @author gamboa on 6/6/20
 */
@XmlRootElement(name = "signer")
@XmlAccessorType(XmlAccessType.FIELD)
public class SignatureInfo {

    @XmlElement(name = "content")
    private SignatureContent contentInfo;

    @XmlElement(name = "timestamp")
    private Timestamp timestamp;

    public SignatureInfo() {
    }


    public Timestamp getTimestamp() {
        return timestamp;
    }

    public SignatureInfo setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public SignatureContent getContentInfo() {
        return contentInfo;
    }

    public SignatureInfo setContentInfo(SignatureContent contentInfo) {
        this.contentInfo = contentInfo;
        return this;
    }
}
