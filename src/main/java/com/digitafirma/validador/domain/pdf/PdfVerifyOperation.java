/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.pdf;

import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.NoSignerException;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;

/**
 * @author gamboa on 6/27/20
 */
public interface PdfVerifyOperation {

    public SignedDocumentValidResult execute(String name, InputStream signedPdf) throws OperatorCreationException, GeneralSecurityException, IOException, NoCertificateException, NoSignerException, TSPException, CMSException, InvalidCertCountException;
}
