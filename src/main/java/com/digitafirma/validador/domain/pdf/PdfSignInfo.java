/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.pdf;

import java.util.Date;

/**
 * @author gamboa on 6/27/20
 */
public class PdfSignInfo {

    private Date signDate;
    private byte[] contentEntry;

    public PdfSignInfo(Date signDate, byte[] contentEntry) {
        this.signDate = signDate;
        this.contentEntry = contentEntry;
    }

    public Date getSignDate() {
        return signDate;
    }

    public byte[] getContentEntry() {
        return contentEntry;
    }
}
