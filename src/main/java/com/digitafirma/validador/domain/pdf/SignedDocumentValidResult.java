/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.pdf;


import com.digitafirma.validador.domain.ValidationResult;

import java.util.List;

/**
 * @author gamboa on 6/21/20
 */
public class SignedDocumentValidResult extends ValidationResult {

    //todo: analizar la opcion de fusionar este modelo de resultado con Result
    // para no mantener 2
    private String filename;
    private String hash;
    private List<SignerResult> signerResults;

    public SignedDocumentValidResult(String name,
                                     String hash,
                                     List<SignerResult> signerResults)
    {
        super();
        this.filename = name;
        this.hash = hash;
        this.signerResults = signerResults;
    }

    public List<SignerResult> getSignerResults() {
        return signerResults;
    }

    public SignedDocumentValidResult setSignerResults(List<SignerResult> signerResults) {
        this.signerResults = signerResults;
        return this;
    }


    public String getFilename() {
        return filename;
    }

    public String getHash() {
        return hash;
    }

}
