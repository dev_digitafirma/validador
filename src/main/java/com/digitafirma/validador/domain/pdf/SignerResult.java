/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.pdf;

import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.xml.Timestamp;
import com.digitafirma.validador.util.json.LocalDateSerializer;
import com.digitafirma.validador.domain.constancy.CertificateResult;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.LocalDateTime;

/**
 * @author gamboa on 6/27/20
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignerResult {

    private String signerName;
    private boolean signatureValid;
    private String digestAlgorithm;
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDateTime signatureDateTime;

    private TimeStampValidationResult timestampResult;

    private CertificateResult certResult;

    public SignerResult(boolean signatureValid,
                        String digestAlgorithm,
                        CertificateResult certResult,
                        String name,
                        LocalDateTime time) {

        this.signatureValid = signatureValid;
        this.digestAlgorithm = digestAlgorithm;
        this.certResult = certResult;
        this.signerName = name;
        this.signatureDateTime = time;

    }

    public SignerResult setTimestampResult(TimeStampValidationResult timestamp) {
        this.timestampResult = timestamp;
        return this;
    }

    public TimeStampValidationResult getTimestampResult() {
        return timestampResult;
    }

    public boolean isSignatureValid() {
        return signatureValid;
    }

    public String getDigestAlgorithm() {
        return digestAlgorithm;
    }

    public CertificateResult getCertResult() {
        return certResult;
    }

    public String getSignerName() {
        return signerName;
    }

    public LocalDateTime getSignatureDateTime() {
        return signatureDateTime;
    }


}
