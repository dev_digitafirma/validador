/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;

import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author gamboa on 5/24/20
 */
public class DocumentDigest {

    private MessageDigest messageDigest;

    private DocumentDigest(MessageDigest messageDigest) {
        this.messageDigest = messageDigest;
    }

    public static DocumentDigest getInstance(DigestAlgorithm digestAlgorithm) throws NoSuchAlgorithmException {
        return new DocumentDigest(digestProvider(digestAlgorithm));
    }

    private static MessageDigest digestProvider(DigestAlgorithm digestAlgorithm) throws NoSuchAlgorithmException {
        MessageDigest md;
        switch (digestAlgorithm) {
            case SHA256:
                md = MessageDigest.getInstance("SHA-256");
                break;
            case SHA512:
                md = MessageDigest.getInstance("SHA-512");
                break;
            case SHA1:
                md = MessageDigest.getInstance("SHA-1");
                break;
            default:
                md = MessageDigest.getInstance("MD5");
        }
        return md;
    }

    public String digestAsString(DigitalDocument document) throws IOException {
        return Hex.toHexString(doDigest(document.getContent()));
    }


    public byte[] digest(DigitalDocument document) throws IOException {
        return doDigest(document.getContent());
    }

    private byte[] doDigest(final byte[] content) throws IOException {
       /* byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        while ((bytesCount = is.read(byteArray)) != -1) {
            messageDigest.update(byteArray, 0, bytesCount);
        }
        is.close();

        return messageDigest.digest();*/
        messageDigest.reset();
        messageDigest.update(content);
        return messageDigest.digest();
    }
}
