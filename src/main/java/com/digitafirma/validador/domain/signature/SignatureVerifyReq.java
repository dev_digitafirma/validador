/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */
package com.digitafirma.validador.domain.signature;

import com.digitafirma.validador.domain.DigestAlgorithm;
import com.digitafirma.validador.domain.DigitalDocument;

import java.security.cert.Certificate;
import java.time.LocalDateTime;

/**
 * @author gamboa on 5/24/20
 */
public class SignatureVerifyReq {

    private DigitalDocument document;
    private byte[] signature;
    private DigestAlgorithm algorithm;
    private Certificate certificate;
    private LocalDateTime signatureDate;


    public SignatureVerifyReq(DigitalDocument document,
                              Certificate cert,
                              byte[] signature,
                              DigestAlgorithm algorithm,
                              LocalDateTime signatureDate) {


        this.document = document;
        this.signature = signature;
        this.certificate = cert;
        this.algorithm = algorithm;
        this.signatureDate = signatureDate;
    }


    public DigitalDocument getDocument() {
        return document;
    }


    public byte[] getSignatureValue() {
        return signature;
    }

    public byte[] getSignature() {
        return signature;
    }

    public DigestAlgorithm getAlgorithm() {
        return algorithm;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public LocalDateTime getSignatureDate() {
        return signatureDate;
    }
}
