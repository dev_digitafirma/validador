/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.signature;

import com.digitafirma.validador.domain.Result;
import com.digitafirma.validador.domain.pdf.SignerResult;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author gamboa on 5/24/20
 */
public interface SignatureVerifyOperation {

    public Result execute(
            InputStream documentIs,
            String documentName,
            InputStream pubKeyCert,
            byte[] signatureValue,
            String algorithm,
            LocalDateTime signatureDate

    ) throws GeneralSecurityException,
            IOException;







}
