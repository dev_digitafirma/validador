/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;

import com.digitafirma.validador.domain.signature.PKIConstant;
import org.bouncycastle.cert.X509CertificateHolder;

import java.io.InputStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * @author gamboa on 5/24/20
 */
public class CertFactory {

    /*public static PublicKey from(PKIConstant format, InputStream certStream) throws CertificateException {
        if (format == PKIConstant.PKCS8) {
            Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(certStream);
            return cert.getPublicKey();
        }
        //todo implements more Formats
        return null;
    }*/

    public static Certificate from(PKIConstant format, InputStream certStream) throws CertificateException {
        if (format == PKIConstant.PKCS8) {
            return  CertificateFactory.getInstance("X.509").generateCertificate(certStream);

        }
        //todo implements more Formats
        return null;
    }
}
