package com.digitafirma.validador.domain;

import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result {

    private String filename;
    private boolean success;
    private String hash;

    private List<SignerResult> signerResults;
    private TimeStampValidationResult tsrResult;

    public Result() {
        this.signerResults = new ArrayList<>();
    }

    public Result addSignerResult(SignerResult signerResult) {
        this.signerResults.add(signerResult);
        return this;
    }

    public Result setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    public Result setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public Result setHash(String hash) {
        this.hash = hash;
        return this;
    }

    public Result setSignerResults(List<SignerResult> signerResults) {
        this.signerResults = signerResults;
        return this;
    }

    public Result setTsrResult(TimeStampValidationResult tsrResult) {
        this.tsrResult = tsrResult;
        return this;
    }

    public String getFilename() {
        return filename;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getHash() {
        return hash;
    }

    public List<SignerResult> getSignerResults() {
        return signerResults;
    }

    public TimeStampValidationResult getTsrResult() {
        return tsrResult;
    }
}
