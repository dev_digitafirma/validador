/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain;

/**
 * Represents an error (Server, client side) response
 * @author gamboa on 5/27/20
 */
public class ErrorResponse {

    private String title;

    private String description;

    private String reference;

    public ErrorResponse(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public ErrorResponse setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getReference() {
        return reference;
    }

    public ErrorResponse setReference(String reference) {
        this.reference = reference;
        return this;
    }
}
