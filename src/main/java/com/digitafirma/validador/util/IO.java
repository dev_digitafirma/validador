/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.util;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author gamboa on 6/21/20
 */
public final class IO {

    public static String  getResourceAsString(InputStream stream){
        return new Scanner(stream, "UTF-8").useDelimiter("\\A").next();
    }

    public static InputStream getResourceAsStream(String path) {
        return IO.class.getClassLoader().getResourceAsStream(path);
    }

    public static Source xsdAsSource(String path)  {
        InputStream resource = IO.class.getClassLoader().getResourceAsStream(path);
        return new StreamSource(resource);
    }


}
