/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.util;

import com.digitafirma.validador.domain.constancy.CertStatus;
import com.digitafirma.validador.domain.constancy.CertificateResult;
import org.apache.pdfbox.pdmodel.encryption.SecurityProvider;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.time.LocalDateTime;

/**
 * @author gamboa on 6/25/20
 */
public final class Certificates {

   /* private void verifyCertificateChain(Store<X509CertificateHolder> certificatesStore,
                                        X509Certificate certFromSignedData, Date signDate)
            throws  CertificateException
    {
        // Verify certificate chain (new since 10/2018)
        // Please post bad PDF files that succeed and
        // good PDF files that fail in
        // https://issues.apache.org/jira/browse/PDFBOX-3017
        Collection<X509CertificateHolder> certificateHolders = certificatesStore.getMatches(null);
        Set<X509Certificate> additionalCerts = new HashSet<>();
        JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
        for (X509CertificateHolder certHolder : certificateHolders)
        {
            X509Certificate certificate = certificateConverter.getCertificate(certHolder);
            if (!certificate.equals(certFromSignedData))
            {
                additionalCerts.add(certificate);
            }
        }
        verifyCertificate(certFromSignedData, additionalCerts, true, signDate);
        //TODO check whether the root certificate is in our trusted list.
        // For the EU, get a list here:
        // https://ec.europa.eu/digital-single-market/en/eu-trusted-lists-trust-service-providers
        // ( getRootCertificates() is not helpful because these are SSL certificates)
    }*/

    /**
     * Checks whether given X.509 certificate is self-signed.
     * @param cert The X.509 certificate to check.
     * @return true if the certificate is self-signed, false if not.
     * @throws java.security.GeneralSecurityException
     */
    public static boolean isSelfSigned(X509Certificate cert) throws GeneralSecurityException
    {
        Security.addProvider(new BouncyCastleProvider());
        try
        {
            // Try to verify certificate signature with its own public key
            PublicKey key = cert.getPublicKey();
            cert.verify(key, SecurityProvider.getProvider().getName());
            return true;
        }
        catch (SignatureException | InvalidKeyException | IOException ex)
        {
            // Invalid signature --> not self-signed
            return false;
        }
    }

    public static String extractSatCertNumber(X509Certificate certificate) {
        BigInteger integer = certificate.getSerialNumber();
        String hex = integer.toString(16);
        StringBuilder noCert = new StringBuilder();
        for (int i = 0; i < hex.length(); i++){
            if ((i & 1 )== 1)
                noCert.append(hex.charAt(i));
        }
        return noCert.toString();
    }


    public static CertificateResult validate(X509Certificate cert, LocalDateTime signatureDate, boolean satStyle) throws GeneralSecurityException {

        String certNo = satStyle
                ? Certificates.extractSatCertNumber(cert)
                : cert.getSerialNumber().toString(16);

        CertStatus certStatus = CertStatus.VALID;
        boolean validOnSignatureDate = true;
        try {
            cert.checkValidity(Dates.toDate(signatureDate));
        } catch (CertificateExpiredException ex) {
            certStatus = CertStatus.EXPIRED;
            validOnSignatureDate = false;
        } catch (CertificateNotYetValidException ex) {
            certStatus = CertStatus.NOT_YET_VALID;
            validOnSignatureDate = false;
        }

        if (Certificates.isSelfSigned(cert))
            certStatus = CertStatus.SELF_SIGNED;

        return new CertificateResult(
                ASNUtil.certIssuerOrganization(cert),
                certNo,
                validOnSignatureDate,
                certStatus
        );
    }

    /**
     * Attempts to build a certification chain for given certificate and to
     * verify it. Relies on a set of root CA certificates and intermediate
     * certificates that will be used for building the certification chain. The
     * verification process assumes that all self-signed certificates in the set
     * are trusted root CA certificates and all other certificates in the set
     * are intermediate certificates.
     *
     * @param cert - certificate for validation
     * @param additionalCerts - set of trusted root CA certificates that will be
     * used as "trust anchors" and intermediate CA certificates that will be
     * used as part of the certification chain. All self-signed certificates are
     * considered to be trusted root CA certificates. All the rest are
     * considered to be intermediate CA certificates.
     * @param verifySelfSignedCert true if a self-signed certificate is accepted, false if not.
     * @param signDate the date when the signing took place
     * @return the certification chain (if verification is successful)
     * @throws CertificateVerificationException - if the certification is not
     * successful (e.g. certification path cannot be built or some certificate
     * in the chain is expired or CRL checks are failed)
     */
   /* public static PKIXCertPathBuilderResult verifyCertificate(
            X509Certificate cert, Set<X509Certificate> additionalCerts,
            boolean verifySelfSignedCert, Date signDate)
            throws SelfSignedException
    {
        try
        {
            // Check for self-signed certificate
            if (!verifySelfSignedCert && isSelfSigned(cert))
            {
                throw new SelfSignedException("The certificate is self-signed.");
            }

            Set<X509Certificate> certSet = CertificateVerifier.downloadExtraCertificates(cert);
            int downloadSize = certSet.size();
            certSet.addAll(additionalCerts);
            if (downloadSize > 0)
            {
                LOG.info("CA issuers: " + (certSet.size() - additionalCerts.size()) + " downloaded certificate(s) are new");
            }

            // Prepare a set of trust anchors (set of root CA certificates)
            // and a set of intermediate certificates
            Set<X509Certificate> intermediateCerts = new HashSet<>();
            Set<TrustAnchor> trustAnchors = new HashSet<>();
            for (X509Certificate additionalCert : certSet)
            {
                if (isSelfSigned(additionalCert))
                {
                    trustAnchors.add(new TrustAnchor(additionalCert, null));
                }
                else
                {
                    intermediateCerts.add(additionalCert);
                }
            }

            if (trustAnchors.isEmpty())
            {
                throw new CertificateVerificationException("No root certificate in the chain");
            }

            // Attempt to build the certification chain and verify it
            PKIXCertPathBuilderResult verifiedCertChain = verifyCertificate(
                    cert, trustAnchors, intermediateCerts, signDate);

            LOG.info("Certification chain verified successfully");

            checkRevocations(cert, certSet, signDate);

            return verifiedCertChain;
        }
        catch (CertPathBuilderException certPathEx)
        {
            throw new CertificateVerificationException(
                    "Error building certification path: "
                            + cert.getSubjectX500Principal(), certPathEx);
        }
        catch (CertificateVerificationException cvex)
        {
            throw cvex;
        }
        catch (IOException | GeneralSecurityException | RevokedCertificateException | OCSPException ex)
        {
            throw new CertificateVerificationException(
                    "Error verifying the certificate: "
                            + cert.getSubjectX500Principal(), ex);
        }
    }
*/


}
