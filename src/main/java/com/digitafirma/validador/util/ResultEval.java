/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.util;

import com.digitafirma.validador.domain.constancy.CertStatus;
import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.pdf.SignedDocumentValidResult;
import com.digitafirma.validador.domain.pdf.SignerResult;

import java.util.List;

/**
 * @author gamboa on 7/1/20
 */
public final class ResultEval {

    public static boolean evalPdfResult(SignedDocumentValidResult result) {
        if (result.getSignerResults() == null)
            return false;

        return evalSignerResults(result.getSignerResults());
    }

    public static boolean evalTsrResult(TimeStampValidationResult result) {
        if (result.getSignerResults() == null)
            return false;

        boolean signersOk = evalSignerResults(result.getSignerResults());

        return signersOk && result.isImprintMessageValid();

    }

    public static boolean evalSignerResults(List<SignerResult> signerResults) {
        for (SignerResult signerResult : signerResults) {
            if (signerResult.isSignatureValid()
                    && signerResult.getCertResult().isValidOnSignatureDate()
                    && signerResult.getCertResult().getCertStatus() == CertStatus.VALID
            ) return true;
        }
        return false;
    }

    public static boolean evalSignerResult(SignerResult signerResult) {
        return  (signerResult.isSignatureValid()
                && signerResult.getCertResult().isValidOnSignatureDate()
                && signerResult.getCertResult().getCertStatus() == CertStatus.VALID
        );
    }
}
