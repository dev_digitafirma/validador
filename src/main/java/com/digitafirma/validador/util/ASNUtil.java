/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.util;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

/**
 * @author gamboa on 5/31/20
 */
public final class ASNUtil {

    public static String certCommonName(X509CertificateHolder cert) {
        return certValue(BCStyle.CN,cert.getSubject());
    }

    public static String certCommonName(X509Certificate cert) throws CertificateEncodingException {
        X500Name x500name = new JcaX509CertificateHolder(cert).getSubject();
        return certValue(BCStyle.CN,x500name);
    }


    private static String certValue(ASN1ObjectIdentifier identifier, X500Name name)
    {
        RDN rdn = name.getRDNs(identifier)[0];
        return IETFUtils.valueToString(rdn.getFirst().getValue());
    }

    public static String certIssuerCommonName(X509CertificateHolder cert) {
        return certValue(BCStyle.CN,cert.getIssuer());
    }

    public static String certIssuerCommonName(X509Certificate cert) throws CertificateEncodingException {
        X500Name x500name = new JcaX509CertificateHolder(cert).getIssuer();
        return certValue(BCStyle.CN,x500name);
    }

    public static String certIssuerOrganization(X509CertificateHolder cert) {
        return certValue(BCStyle.O,cert.getIssuer());
    }

    public static String certIssuerOrganization(X509Certificate cert) throws CertificateEncodingException {
        X500Name x500name = new JcaX509CertificateHolder(cert).getIssuer();
        return certValue(BCStyle.O,x500name);
    }

    public static TimeStampToken buildTsrToken(byte[] asnStamp) throws IOException, TSPException {
        ASN1InputStream input = new ASN1InputStream(asnStamp);
        return new TimeStampResponse(input).getTimeStampToken();
    }



    public static TimeStampToken timeStampTokenFrom(SignerInformation signerInformation)
            throws IOException, CMSException, TSPException {

        if (signerInformation.getUnsignedAttributes() == null)
            return null;

        AttributeTable unsignedAttr = signerInformation.getUnsignedAttributes();
        Attribute attr = unsignedAttr.get(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken);

        if (attr == null)
            return null;

        ASN1Object obj = (ASN1Object) attr.getAttrValues().getObjectAt(0);
        CMSSignedData token = new CMSSignedData(obj.getEncoded());
        return new TimeStampToken(token);
    }

    /*public static boolean validateTimeStampToken(TimeStampToken token) {

    }*/
}
