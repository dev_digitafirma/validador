package com.digitafirma.validador.util.xml;

import com.digitafirma.validador.domain.xml.XmlSummary;
import com.digitafirma.validador.util.IO;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.InputStream;

public class UnmarshallerUtil {

    public static XmlSummary unMarshallXmlSummary(InputStream xmlContent) throws JAXBException, SAXException {
        JAXBContext jaxbContext = JAXBContext.newInstance(XmlSummary.class);

        Unmarshaller jaxbUnmarshal = jaxbContext.createUnmarshaller();

        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema digitafirmaSchema = sf.newSchema(IO.xsdAsSource("digitafirma.xsd"));
        jaxbUnmarshal.setSchema(digitafirmaSchema);

        return (XmlSummary) jaxbUnmarshal.unmarshal(xmlContent);
    }
}
