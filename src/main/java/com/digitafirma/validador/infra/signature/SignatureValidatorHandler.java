/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.signature;

import com.digitafirma.validador.domain.Result;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.digitafirma.validador.domain.signature.SignatureValidationResult;
import com.digitafirma.validador.domain.signature.SignatureVerifyOperation;
import com.digitafirma.validador.util.Dates;
import io.javalin.http.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Objects;

/**
 * Represents a Signature Validator action
 *
 * @author gamboa on 5/24/20
 */
public class SignatureValidatorHandler implements Handler {

    private SignatureVerifyOperation operation;

    public SignatureValidatorHandler(SignatureVerifyOperation operation) {
        this.operation = operation;
    }


    @Override
    public void handle(@NotNull Context context) throws GeneralSecurityException {

        UploadedFile digitalDocument = context.uploadedFile("document");
        if (digitalDocument == null)
           throw new BadRequestResponse("document is required");

        UploadedFile cert = context.uploadedFile("cert");
        if (cert == null)
            throw new BadRequestResponse("cert is required");

        String algorithm = context.formParam("algorithm");

        if(algorithm == null || algorithm.isEmpty())
            throw new BadRequestResponse("algorithm is required");

        LocalDateTime signatureLocalDate;

        String signatureDate = context.formParam("signatureDate");

        if(signatureDate == null)
            signatureLocalDate = LocalDateTime.now();
        else
            signatureLocalDate = LocalDateTime.parse(signatureDate,DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        InputStream documentIs = digitalDocument.getContent();
        String documentName = digitalDocument.getFilename();
        InputStream certIs = cert.getContent();
        String signatureStrValue = context.formParam("signature", String.class).check(Objects::nonNull).get();
        byte[] signatureValue = Base64.getDecoder().decode(signatureStrValue);

        Result operationResult;
        try {
            operationResult = operation.execute(documentIs,
                    documentName,
                    certIs,
                    signatureValue,
                    algorithm,
                    signatureLocalDate
            );

        } catch (InvalidKeySpecException
                | CertificateException
                | InvalidKeyException
                | SignatureException
                | InvalidAlgorithmParameterException
                | IOException e) {

            throw new BadRequestResponse(e.getMessage());

        }
        context.json(operationResult);
    }

}
