/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.signature;

import com.digitafirma.validador.domain.DigestAlgorithm;
import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.constancy.CertStatus;
import com.digitafirma.validador.domain.constancy.CertificateResult;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.domain.signature.SignatureVerifyReq;
import com.digitafirma.validador.util.ASNUtil;
import com.digitafirma.validador.util.Certificates;
import com.digitafirma.validador.util.Dates;
import org.apache.pdfbox.pdmodel.encryption.SecurityProvider;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;

import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;

/**
 * @author gamboa on 5/24/20
 */
public class SignatureValidatorImpl implements SignatureValidator {


    @Override
    public SignerResult validate(SignatureVerifyReq req)
            throws GeneralSecurityException {

        Signature signature;

        switch (req.getAlgorithm())
        {
            case SHA512:  signature = Signature.getInstance("SHA512withRSA"); break;
            case MD5:  signature = Signature.getInstance("MD5withRSA"); break;
            case SHA1: signature = Signature.getInstance("SHA1withRSA"); break;
            default: signature = Signature.getInstance("SHA256withRSA"); break;
        }



        signature.initVerify(req.getCertificate());
        signature.update(req.getDocument().getContent());

        X509Certificate cert = (X509Certificate) req.getCertificate();


        boolean isValidSignature = signature.verify(req.getSignatureValue());
        CertificateResult certResult = Certificates.validate(cert,req.getSignatureDate(),true);

        return new SignerResult(
                isValidSignature,
                req.getAlgorithm().getName(),
                certResult,
                ASNUtil.certCommonName(cert),
                req.getSignatureDate()
        );

    }

    @Override
    @SuppressWarnings("unchecked")
    public SignerResult validatePKCS7(SignerInformation signerInformation,
                                      Store<X509CertificateHolder> certs,
                                      Date signDate,
                                      boolean satStyle)
            throws
            NoCertificateException,
            GeneralSecurityException,
            IOException,
            OperatorCreationException,
            CMSException
    {
        //find the certificates of the signer
        Collection<X509CertificateHolder> matches =
                certs.getMatches((Selector<X509CertificateHolder>) signerInformation.getSID());


        if (matches.isEmpty()) {
            throw new NoCertificateException("no certs found for the signer"
                    + signerInformation.getSID().getIssuer());
        }

        X509CertificateHolder certificateHolder = matches.iterator().next();
        X509Certificate certFromSignedData = new JcaX509CertificateConverter()
                .getCertificate(certificateHolder);

        LocalDateTime signatureDateTime = Dates.toLocalDateTime(signDate);


        CertificateResult certResult = Certificates.validate(
                certFromSignedData,
                signatureDateTime,
                true
        );

        boolean signatureValid = signerInformation.verify(new JcaSimpleSignerInfoVerifierBuilder().
                setProvider(SecurityProvider.getProvider()).build(certFromSignedData));

        /*System.out.println(Hex.toHexString(signerInformation.getContentDigest()));*/

        return new SignerResult(
                signatureValid,
                DigestAlgorithm.valueOfOid(signerInformation.getDigestAlgOID()).getName(),
                certResult,
                ASNUtil.certCommonName(certFromSignedData),
                signatureDateTime
        );

    }


}
