/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.pdf;

import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.NoSignerException;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.domain.pdf.SignedDocumentValidResult;
import com.digitafirma.validador.domain.pdf.PdfVerifyOperation;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.http.UploadedFile;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * @author gamboa on 6/27/20
 */
public class PdfValidatorHandler implements Handler {

    private PdfVerifyOperation operation;

    public PdfValidatorHandler(PdfVerifyOperation operation) {
        this.operation = operation;
    }

    @Override
    public void handle(@NotNull Context context)
            throws GeneralSecurityException,
            OperatorCreationException,
            IOException {


        UploadedFile file = context.uploadedFile("pdf");

        if (file == null)
            throw new BadRequestResponse("pdf no puede ser null");

        if (!file.getContentType().equals("application/pdf") || !file.getExtension().equals(".pdf"))
            throw new BadRequestResponse("el archivo no es un PDF válido");

        try {
            SignedDocumentValidResult result = operation.execute(file.getFilename(),file.getContent()) ;
            context.json(result);
        }catch (NoSignerException e){
            throw new BadRequestResponse("No se encontró ningún firmante en el PDF. " + e.getMessage());
        }catch (CMSException e){
            throw new BadRequestResponse("La estructura de los firmantes es inválida o no acorde " +
                    "a la especificación PAdES. " + e.getMessage()
            );
        }catch (NoCertificateException | InvalidCertCountException e){
            throw new BadRequestResponse("Se esperaba almenos un certificado X509. " + e.getMessage());
        }catch (TSPException e){
            throw new BadRequestResponse("Constancia malformada");
        }



    }
}
