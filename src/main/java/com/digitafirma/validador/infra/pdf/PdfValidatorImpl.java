/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.pdf;

import com.digitafirma.validador.domain.*;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.constancy.TimeStampValidator;
import com.digitafirma.validador.domain.constancy.TimeStampVerifyReq;
import com.digitafirma.validador.domain.pdf.*;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.util.ASNUtil;
import com.digitafirma.validador.util.ResultEval;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.*;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Store;
import org.eclipse.jetty.util.log.Log;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author gamboa on 6/27/20
 */
public class PdfValidatorImpl implements PdfValidator {

    private SignatureValidator signatureValidator;
    private TimeStampValidator stampValidator;

    public PdfValidatorImpl(SignatureValidator signatureValidator, TimeStampValidator stampValidator) {
        this.signatureValidator = signatureValidator;
        this.stampValidator = stampValidator;

    }


    @Override
    public SignedDocumentValidResult validate(PdfVerifyReq req) throws IOException, GeneralSecurityException, OperatorCreationException, NoCertificateException, NoSignerException, TSPException, CMSException, InvalidCertCountException {


        List<SignerResult> signerResults = new ArrayList<>();
        DocumentDigest documentDigest = DocumentDigest.getInstance(DigestAlgorithm.SHA256);
        String hash = documentDigest.digestAsString(req.getSignedData());

        if (req.getPdfSignInfoList() == null) {
            PDDocument pdDocument = PDDocument.load(req.getSignedData().getContent());
            // Signature Dictionary

            List<PDSignature> pdSignatures = pdDocument.getSignatureDictionaries();

            if (pdSignatures.size() == 0) {
                pdDocument.close();
                throw new NoSignerException("The PDF does not contain signatures");
            }

            for (PDSignature signature : pdSignatures) {

                COSDictionary sigDict = signature.getCOSObject();
                // de momento se validar si un pdf esta firmado , atrapando una excepcion,
                // me parece que esto se puede hacer mejor , buscando en el diccionario del PDF
                byte[] signedAttr;
                try {
                    signedAttr = signature.getSignedContent(req.getSignedData().getContent()); /////////

                } catch (NullPointerException e) {
                    pdDocument.close();
                    throw new NoSignerException("The PDF was not signed correctly");
                }
                // /Content Entry in Signature Dictionary
                COSString contents = (COSString) sigDict.getDictionaryObject(COSName.CONTENTS);
                Date signDate = signature.getSignDate().getTime();

                signerResults.add(doValidation(
                        signedAttr,
                        contents.getBytes(),
                        signDate,
                        true)
                );
                pdDocument.close();
            }

        } else {
            for (PdfSignInfo pdfInfo : req.getPdfSignInfoList())

                signerResults.add(doValidation(req.getSignedData().getContent()
                        , pdfInfo.getContentEntry()
                        , pdfInfo.getSignDate()
                        , false)
                );
        }
        SignedDocumentValidResult result = new SignedDocumentValidResult(
                req.getSignedData().getName(),
                hash,
                signerResults
        );

        boolean isOk = ResultEval.evalPdfResult(result);
        result.setSuccess(isOk);
        return result;

    }

    private SignerResult doValidation(byte[] signedData, byte[] contents, Date signDate, boolean pades)
            throws CMSException, NoCertificateException, NoSignerException,
            GeneralSecurityException, IOException, TSPException,
            OperatorCreationException, InvalidCertCountException {

        CMSProcessable signedContent = new CMSProcessableByteArray(signedData);
        //RFC3852 - PKCS#7 binary data object
        CMSSignedData asnContents = new CMSSignedData(signedContent, contents);
        // Nom156Validator nom156Validator = new Nom156Validator();

        Store<X509CertificateHolder> certificatesStore = asnContents.getCertificates();
        if (certificatesStore.getMatches(null).isEmpty()) {
            throw new NoCertificateException("No certificates in signature");
        }

        Collection<SignerInformation> signers = asnContents.getSignerInfos().getSigners();

        if (signers.isEmpty()) {
            throw new NoSignerException("No signers in signature");
        }

        SignerInformation signerInformation = signers.iterator().next();

        SignerResult signerResult = signatureValidator.validatePKCS7(
                signerInformation,
                certificatesStore,
                signDate,
                true
        );

        if(pades){
            TimeStampToken timeStampToken = ASNUtil.timeStampTokenFrom(signerInformation);

            // no todos los PAdES tienen timestamps en las firmas
            if(timeStampToken != null) {
                TimeStampVerifyReq stampVerifyReq = new TimeStampVerifyReq(
                        timeStampToken,
                        new DigitalDocument("padesSignature",signerInformation.getSignature())
                );
                TimeStampValidationResult stampValidationResult = stampValidator.validate(stampVerifyReq);
                signerResult.setTimestampResult(stampValidationResult);
            }

        }

        return signerResult;
    }
}
