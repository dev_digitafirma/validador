/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.xml;

import com.digitafirma.validador.domain.*;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.domain.constancy.TimeStampValidationResult;
import com.digitafirma.validador.domain.constancy.TimeStampValidator;
import com.digitafirma.validador.domain.constancy.TimeStampVerifyReq;
import com.digitafirma.validador.domain.pdf.SignedDocumentValidResult;
import com.digitafirma.validador.domain.pdf.PdfValidator;
import com.digitafirma.validador.domain.pdf.PdfVerifyReq;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.digitafirma.validador.domain.signature.PKIConstant;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.domain.signature.SignatureVerifyReq;
import com.digitafirma.validador.domain.xml.Signer;
import com.digitafirma.validador.domain.xml.XmlSummary;
import com.digitafirma.validador.domain.xml.XmlValidationResult;
import com.digitafirma.validador.domain.xml.XmlValidator;
import com.digitafirma.validador.util.ResultEval;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.crypto.tls.Certificate;
import org.bouncycastle.jcajce.provider.asymmetric.x509.CertificateFactory;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author gamboa on 6/6/20
 */
public class XmlValidatorImpl implements XmlValidator {

    private PdfValidator pdfValidator;
    private SignatureValidator signatureValidator;
    private TimeStampValidator timeStampValidator;

    public XmlValidatorImpl(PdfValidator pdfValidator,
                            TimeStampValidator timeStampValidator,
                            SignatureValidator signatureValidator) {

        this.pdfValidator = pdfValidator;
        this.timeStampValidator = timeStampValidator;
        this.signatureValidator = signatureValidator;
    }

    @Override
    public XmlValidationResult validate(XmlSummary xmlSummary)
            throws GeneralSecurityException,
            IOException,
            TSPException,
            CMSException,
            OperatorCreationException,
            InvalidCertCountException, NoCertificateException, NoSignerException {



        DigitalDocument document = xmlSummary.getDocument();

        DigestAlgorithm algorithm = xmlSummary
                .getSigners()
                .get(0)
                .getSignatureInfo()
                .getContentInfo()
                .getAlgorithm();

        /*
        * Validar nom-151
        * */
        TimeStampVerifyReq nomVerifyReq =  new TimeStampVerifyReq(
                document,
                xmlSummary.getNom151().getTsr()
        );

        SignedDocumentValidResult documentValidResult;



        if(xmlSummary.isPdfSignature()) {
            PdfVerifyReq pdfReq = new PdfVerifyReq(document);
            documentValidResult = pdfValidator.validate(pdfReq);

        }else {

            List<SignerResult> signerResults = new ArrayList<>();
            for (Signer signer: xmlSummary.getSigners()) {
                InputStream certStream = new ByteArrayInputStream(signer.getCertificate().getContent());

                SignatureVerifyReq req = new SignatureVerifyReq(
                        document,
                        CertFactory.from(PKIConstant.PKCS8,certStream),
                        signer.getSignatureInfo().getContentInfo().getSignatureContent(),
                        signer.getSignatureInfo().getContentInfo().getAlgorithm(),
                        signer.getSignatureInfo().getTimestamp().getDateTime()
                );
                DigitalDocument signatureDocument = new DigitalDocument(
                        "signature",signer.getSignatureInfo().getContentInfo().getSignatureContent()
                );

                TimeStampVerifyReq signatureStampReq = new TimeStampVerifyReq(
                        signatureDocument,
                        signer.getSignatureInfo().getTimestamp().getTsr()
                );

                TimeStampValidationResult stampValidationResult = timeStampValidator.validate(signatureStampReq);

                signerResults.add(
                        signatureValidator.validate(req).setTimestampResult(stampValidationResult)
                );
            }

            String hash = DocumentDigest.getInstance(algorithm).digestAsString(document);
            documentValidResult = new SignedDocumentValidResult(document.getName(), hash,signerResults);

        }

        boolean isOk = ResultEval.evalPdfResult(documentValidResult);

        TimeStampValidationResult nom151Result = timeStampValidator.validate(nomVerifyReq);
        documentValidResult.setSuccess(isOk);
        // TODO: 11/18/20 hacer enum el tipo de firmas aplicadas en un XML
        return new XmlValidationResult(
                documentValidResult,
                nom151Result,
                xmlSummary.isPdfSignature() ? "Firma Electrónica Avanzada PAdES (PDF) " : "Firma Electrónica Avanzada "
        );
    }

}
