/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.xml;

import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.NoSignerException;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.domain.xml.XmlValidationResult;
import com.digitafirma.validador.domain.xml.XmlVerifyOperation;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.http.UploadedFile;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * @author gamboa on 5/24/20
 */
public class XmlValidatorHandler implements Handler {

    private XmlVerifyOperation xmlVerifyOperation;

    public XmlValidatorHandler(XmlVerifyOperation operation) {
        xmlVerifyOperation = operation;
    }

    @Override
    public void handle(@NotNull Context context)
            throws IOException, GeneralSecurityException,
            OperatorCreationException{

        UploadedFile xml = context.uploadedFile("xml");

        if (xml == null)
            throw new BadRequestResponse("xml is required");

        /*if(!xml.getContentType().equals("application/xml") || !xml.getExtension().equals(".xml"))
            throw new BadRequestResponse("invalid file provided");*/


        try {
            XmlValidationResult result = xmlVerifyOperation.execute(xml.getContent());
            context.json(result);
        } catch (JAXBException | SAXException e) {
            throw new BadRequestResponse("El XML no fue emitido por DigitaFirma o es un XML inválido.");
        } catch (TSPException e) {
            throw new BadRequestResponse("El XML contiene una constancia inválida.");
        } catch (InvalidCertCountException | NoCertificateException e) {
            throw new BadRequestResponse("Se esperaba almenos un certificado X509. " + e.getMessage());
        } catch (NoSignerException e) {
            throw new BadRequestResponse("No se encontró ningún firmante en el archivo. " + e.getMessage());
        } catch (CMSException e) {
            throw new BadRequestResponse("La estructura de firmantes es inválida. " + e.getMessage());
        }
    }
}
