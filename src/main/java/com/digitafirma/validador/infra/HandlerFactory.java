/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra;

import com.digitafirma.validador.app.constancy.Nom151VerifyOperation;
import com.digitafirma.validador.app.pdf.PdfVerifyOperationImpl;
import com.digitafirma.validador.app.signature.SignatureVerifyOperationImpl;
import com.digitafirma.validador.app.xml.XmlVerifyOperationImpl;
import com.digitafirma.validador.domain.Routes;
import com.digitafirma.validador.domain.constancy.TimeStampValidator;
import com.digitafirma.validador.domain.constancy.TimeStampVerifyOperation;
import com.digitafirma.validador.domain.pdf.PdfValidator;
import com.digitafirma.validador.domain.pdf.PdfVerifyOperation;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.domain.signature.SignatureVerifyOperation;
import com.digitafirma.validador.domain.xml.XmlValidator;
import com.digitafirma.validador.infra.constancy.ConstancyValidatorHandler;
import com.digitafirma.validador.infra.constancy.Nom151Validator;
import com.digitafirma.validador.infra.pdf.PdfValidatorHandler;
import com.digitafirma.validador.infra.pdf.PdfValidatorImpl;
import com.digitafirma.validador.infra.signature.SignatureValidatorHandler;
import com.digitafirma.validador.infra.signature.SignatureValidatorImpl;
import com.digitafirma.validador.infra.xml.XmlValidatorImpl;
import com.digitafirma.validador.infra.xml.XmlValidatorHandler;
import io.javalin.http.Handler;
import kotlin.NotImplementedError;

/**
 * @author gamboa on 7/4/20
 */
public final class HandlerFactory {

    public static Handler fromRoute(String route)
    {
        switch (route) {
            case Routes.API.XML: return xmlHandler();
            case Routes.API.CONSTANCY: return constancyHandler();
            case Routes.API.PDF: return pdfHandler();
            case Routes.API.SIGN: return signatureHandler();
            default: throw new NotImplementedError();
        }
    }

    private static ConstancyValidatorHandler constancyHandler() {

        TimeStampVerifyOperation timestampOperation = new Nom151VerifyOperation(stampValidator());
        return new ConstancyValidatorHandler(timestampOperation);
    }

    private static XmlValidatorHandler xmlHandler() {
        return new XmlValidatorHandler(new XmlVerifyOperationImpl(xmlValidator()));
    }

    private static SignatureValidatorHandler signatureHandler() {

        return new SignatureValidatorHandler(
                new SignatureVerifyOperationImpl(signatureValidator())
        );
    }

    private static PdfValidatorHandler pdfHandler() {
        PdfVerifyOperation operation = new PdfVerifyOperationImpl(pdfValidator());
        return new PdfValidatorHandler(operation);
    }

    private static SignatureValidator signatureValidator() {
        return new SignatureValidatorImpl();
    }

    private static PdfValidator pdfValidator() {
        return new PdfValidatorImpl(signatureValidator(), stampValidator());
    }

    private static TimeStampValidator stampValidator() {
        return new Nom151Validator(signatureValidator());
    }

    private static XmlValidator xmlValidator() {

        return new XmlValidatorImpl(
                pdfValidator(),
                stampValidator(),
                signatureValidator()
        );
    }
}
