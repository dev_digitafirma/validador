/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.constancy;

import com.digitafirma.validador.domain.DigestAlgorithm;
import com.digitafirma.validador.domain.DigitalDocument;
import com.digitafirma.validador.domain.DocumentDigest;
import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.constancy.*;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.util.ResultEval;
import org.apache.pdfbox.pdmodel.encryption.SecurityProvider;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.*;

/**
 * @author gamboa on 5/30/20
 */
public class Nom151Validator implements TimeStampValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Nom151Validator.class);
    private SignatureValidator signatureValidator;



    public Nom151Validator(SignatureValidator signatureValidator) {
        this.signatureValidator = signatureValidator;
    }

    @Override
    public TimeStampValidationResult validate(TimeStampVerifyReq req) throws
            GeneralSecurityException,
            IOException,
            OperatorCreationException,
            InvalidCertCountException,
            CMSException, NoCertificateException {


        List<SignerResult> certResults = checkCert(req.getStampToken());

        byte[] imprint = req
                .getStampToken()
                .getTimeStampInfo()
                .getMessageImprintDigest();

        String hashAlgorithm = req
                .getStampToken()
                .getTimeStampInfo()
                .getMessageImprintAlgOID()
                .getId();

        boolean impMsjStatus = checkImprintMessage(req.getDocument(), imprint, hashAlgorithm);

        String policy = req
                .getStampToken()
                .getTimeStampInfo()
                .getPolicy()
                .toString();

        TimeStampValidationResult result = new TimeStampValidationResult(
                certResults,
                policy,
                impMsjStatus,
                Hex.toHexString(imprint));

        boolean isResultOk = ResultEval.evalTsrResult(result);
        result.setSuccess(isResultOk);
        return result;

    }


    private List<SignerResult> checkCert(TimeStampToken token) throws
            InvalidCertCountException,
            GeneralSecurityException,
            OperatorCreationException,
            CMSException, NoCertificateException, IOException {

        List<SignerResult> results = new ArrayList<>();

        Store<X509CertificateHolder> certs = token
                .toCMSSignedData()
                .getCertificates();

        Collection<SignerInformation> signers = token
                .toCMSSignedData()
                .getSignerInfos()
                .getSigners();

        Date timestamp = token
                .getTimeStampInfo()
                .getGenTime();


        for (SignerInformation signer : signers) { //todo: just use one signer

            Collection<X509CertificateHolder> signerCerts = certs.getMatches(signer.getSID());

            if (signerCerts.size() != 1)
                throw new InvalidCertCountException("One cert per signer is expected");

            results.add(signatureValidator.validatePKCS7(signer, certs, timestamp, false));
        }

        return results;
    }

    //todo : pasar DIGEST ALGO OID
    private boolean checkImprintMessage(DigitalDocument document, byte[] imprint, String hashAlgorithm)
            throws NoSuchAlgorithmException, IOException {

        DocumentDigest documentDigest = DocumentDigest.getInstance(
                DigestAlgorithm.valueOfOid(hashAlgorithm)
        );

        byte[] hash = documentDigest.digest(document);
        return Arrays.equals(hash, imprint);
    }

    public boolean validateTimestampToken(TimeStampToken timeStampToken)
            throws TSPException, CertificateException, OperatorCreationException, IOException {

        @SuppressWarnings("unchecked") // TimeStampToken.getSID() is untyped
                Collection<X509CertificateHolder> tstMatches =
                timeStampToken.getCertificates().getMatches(timeStampToken.getSID());
        X509CertificateHolder certificateHolder = tstMatches.iterator().next();

        SignerInformationVerifier siv = new JcaSimpleSignerInfoVerifierBuilder()
                        .setProvider(SecurityProvider.getProvider())
                        .build(certificateHolder);

        return timeStampToken.isSignatureValid(siv);
    }
}
