/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.infra.constancy;

import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.Result;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.domain.constancy.TimeStampVerifyOperation;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.http.UploadedFile;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;


/**
 * @author gamboa on 5/24/20
 */
public class ConstancyValidatorHandler implements Handler {

    private TimeStampVerifyOperation verifyOperation;

    public ConstancyValidatorHandler(TimeStampVerifyOperation verifyOperation) {
        this.verifyOperation = verifyOperation;
    }

    @Override
    public void handle(@NotNull Context context) throws OperatorCreationException {

        UploadedFile document = context.uploadedFile("document");
        String constancy = context.formParam("nom151");

        if (document == null)
            throw new BadRequestResponse("document is required");

        if (constancy == null)
            throw new BadRequestResponse("The nom151 is required");

        Result result;
        try {
            result = verifyOperation.execute(
                    document.getContent(),
                    document.getFilename(),
                    constancy
            );
        }
        // bad files, bad constancy
        catch (InvalidCertCountException e) {
            throw new BadRequestResponse(e.getMessage());
        } catch (TSPException e) {
            throw new BadRequestResponse("Constancia malformada , posible TSR incorrecto");
        } catch (CertificateException e) {
            throw new BadRequestResponse("La constancia contiene un certificado malformado");
        } catch (NoCertificateException e) {
            throw new BadRequestResponse("No se encontraron certificados en la constancia");
        } catch (GeneralSecurityException e) {
            throw new BadRequestResponse("Cannot continue"); // :(
        } catch (CMSException e) {
            throw new BadRequestResponse("Constancia inválida, no sigue una estructura CMS"); // :(
        }catch (IOException e) {
            throw new BadRequestResponse("Entrada Base64 inválida, verifique"); // :(
        }

        context.json(result);
    }


}
