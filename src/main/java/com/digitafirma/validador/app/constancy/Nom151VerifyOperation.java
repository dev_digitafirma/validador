/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.app.constancy;

import com.digitafirma.validador.domain.*;
import com.digitafirma.validador.domain.constancy.*;
import com.digitafirma.validador.infra.constancy.Nom151Validator;
import com.digitafirma.validador.util.ASNUtil;
import com.digitafirma.validador.util.ResultEval;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Base64;

/**
 * Represents a NOM-151 Verify operation (actually, a NOM-151 follows the RFC 3161, so is
 * an TimeStamp Operation
 * @author gamboa on 5/30/20
 */
public class Nom151VerifyOperation implements TimeStampVerifyOperation {

    private TimeStampValidator validator;

    public Nom151VerifyOperation(TimeStampValidator validator){
        this.validator = validator;
    }

    @Override
    public Result execute(InputStream document, String documentName, String constancy) throws
            IOException,
            GeneralSecurityException,
            OperatorCreationException,
            InvalidCertCountException,
            CMSException,
            NoCertificateException,
            TSPException {

        DigitalDocument digitalDocument = new DigitalDocument(
                documentName,
                IOUtils.toByteArray(document)
        );

        TimeStampToken stampToken = ASNUtil.buildTsrToken(
                Base64.getDecoder().decode(constancy)
        );
        String hash = DocumentDigest.getInstance(DigestAlgorithm.SHA256).digestAsString(digitalDocument);

        TimeStampValidationResult tsrResult = validator.validate(
                new TimeStampVerifyReq(stampToken,digitalDocument)
        );

        boolean isOk = ResultEval.evalTsrResult(tsrResult);

        return new Result()
                .setTsrResult(tsrResult)
                .setFilename(digitalDocument.getName())
                .setHash(hash)
                .setSuccess(isOk);
    }
}
