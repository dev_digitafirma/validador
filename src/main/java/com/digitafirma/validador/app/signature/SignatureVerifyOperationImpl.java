/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.app.signature;

import com.digitafirma.validador.domain.*;
import com.digitafirma.validador.domain.pdf.SignerResult;
import com.digitafirma.validador.domain.signature.*;
import com.digitafirma.validador.infra.signature.SignatureValidatorImpl;
import com.digitafirma.validador.util.ResultEval;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.time.LocalDateTime;

/**
 * @author gamboa on 5/24/20
 */
public class SignatureVerifyOperationImpl implements SignatureVerifyOperation {

    private SignatureValidator validator;

    public SignatureVerifyOperationImpl(SignatureValidator validator) {
        this.validator = validator;
    }

    @Override
    public Result execute(
            InputStream documentIs,
            String documentName,
            InputStream pubKeyCert,
            byte[] signatureValue,
            String algorithm,
            LocalDateTime  signatureDate


    ) throws GeneralSecurityException,
            IOException {

        DigestAlgorithm digestAlgorithm;
        try {
            digestAlgorithm = DigestAlgorithm.valueOf(algorithm);
        } catch (IllegalArgumentException e) {
            throw new InvalidAlgorithmParameterException("The Algorithm is not supported");
        }

        DigitalDocument document = new DigitalDocument(documentName, IOUtils.toByteArray(documentIs));
        Certificate cert = CertFactory.from(PKIConstant.PKCS8,pubKeyCert);
        String hash = DocumentDigest.getInstance(digestAlgorithm).digestAsString(document);

        SignatureVerifyReq req = new SignatureVerifyReq(
                document,
                cert,
                signatureValue,
                digestAlgorithm,
                signatureDate
        );

        SignerResult signerResult = validator.validate(req);
        boolean isOk = ResultEval.evalSignerResult(signerResult);

        return new Result()
                .setFilename(document.getName())
                .setHash(hash)
                .addSignerResult(signerResult)
                .setSuccess(isOk);
    }
}
