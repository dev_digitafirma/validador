/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador;

import com.digitafirma.validador.infra.HandlerFactory;
import com.digitafirma.validador.infra.pdf.PdfValidatorHandler;
import com.digitafirma.validador.domain.ErrorResponse;
import com.digitafirma.validador.domain.Routes;
import com.digitafirma.validador.infra.constancy.ConstancyValidatorHandler;
import com.digitafirma.validador.infra.signature.SignatureValidatorHandler;
import com.digitafirma.validador.infra.xml.XmlValidatorHandler;
import io.javalin.Javalin;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.rendering.vue.VueComponent;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eclipse.jetty.http.HttpStatus;

import java.security.Security;

public class ValidadorApp {

    public static void main(String[] args) {

        Security.addProvider(new BouncyCastleProvider());



        Javalin app = Javalin.create(config -> {
            config.enableCorsForAllOrigins();
            config.addSinglePageRoot("/", "/static/index.html",Location.CLASSPATH);
            config.addStaticFiles("/","/static",Location.CLASSPATH);
        }).start(8080);


        //override default BadRequestResponse
        app.exception(BadRequestResponse.class, (e, ctx) -> {
            ctx.status(HttpStatus.BAD_REQUEST_400);
            ErrorResponse response = new ErrorResponse("Bad Request")
                .setDescription(e.getMessage())
                .setReference("https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400");
            ctx.json(response);
        });

        //override default InternalServerErrorResponse
        app.exception(InternalServerErrorResponse.class, (e, ctx) -> {
            ctx.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            ErrorResponse response = new ErrorResponse("Server Error")
                .setDescription(e.getMessage())
                .setReference("ups, we are really sorry , please contact us :(");
            ctx.json(response);
        });



        //VIEW
       /* app.get("/",new VueComponent("<home></home>"));
        app.get("/firma", new VueComponent("<signature></signature>"));*/


        //API
        app.post(Routes.API.SIGN, HandlerFactory.fromRoute(Routes.API.SIGN));

        app.post(Routes.API.CONSTANCY, HandlerFactory.fromRoute(Routes.API.CONSTANCY));

        app.post(Routes.API.XML, HandlerFactory.fromRoute(Routes.API.XML));

        app.post(Routes.API.PDF, HandlerFactory.fromRoute(Routes.API.PDF));



        /*app.post(Routes.View.SIGNATURE, ctx -> {
            ctx.html("Your reservation day is "+ctx.formParam("day") );
        });
*/
    }
}
