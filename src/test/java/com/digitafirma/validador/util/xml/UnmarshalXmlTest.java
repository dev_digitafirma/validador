package com.digitafirma.validador.util.xml;

import com.digitafirma.validador.domain.xml.XmlSummary;
import com.digitafirma.validador.util.IO;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class UnmarshalXmlTest {

    @Test
    public void nonNullOnUnmarshalTest() throws JAXBException, SAXException {

        XmlSummary xmlSummary = UnmarshallerUtil.unMarshallXmlSummary(
                IO.getResourceAsStream("xml/digitafirma.xml")
        );
        Assert.assertNotNull(xmlSummary);
    }

    @Test(expected = JAXBException.class)
    public void throwsJaxbExceptionWhenSchemaIsInvalid() throws JAXBException, SAXException {
        XmlSummary xmlSummary = UnmarshallerUtil.unMarshallXmlSummary(
                IO.getResourceAsStream("xml/invalidSchema.xml")
        );
    }

    @Test
    public void canUnmarshalXmlWithClassicSignatures() {

    }
}
