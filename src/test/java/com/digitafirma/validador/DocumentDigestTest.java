/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador;

import com.digitafirma.validador.domain.DigestAlgorithm;
import com.digitafirma.validador.domain.DigitalDocument;
import com.digitafirma.validador.domain.DocumentDigest;
import com.digitafirma.validador.util.IO;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Base64;

/**
 * @author gamboa on 6/13/20
 */
public class DocumentDigestTest {

    @Test
    public void digestTest() throws NoSuchAlgorithmException, IOException {
        Security.addProvider(new BouncyCastleProvider());
        DocumentDigest dd = DocumentDigest.getInstance(DigestAlgorithm.SHA256);
        String content = IO.getResourceAsString(getClass().getResourceAsStream("/pdf.b64"));

        DigitalDocument document = new DigitalDocument("gamboa",content.getBytes());

        Assert.assertEquals(
                "d4c85b9e99ba186ca146d93b5ba274020e7b1b2cb965d962869bb7a9d8af0908",
                dd.digestAsString(document)
        );

    }
}
