package com.digitafirma.validador.domain;

import com.digitafirma.validador.app.pdf.PdfVerifyOperationImpl;
import com.digitafirma.validador.app.signature.SignatureVerifyOperationImpl;
import com.digitafirma.validador.app.xml.XmlVerifyOperationImpl;
import com.digitafirma.validador.domain.constancy.TimeStampValidator;
import com.digitafirma.validador.domain.pdf.PdfValidator;
import com.digitafirma.validador.domain.pdf.PdfVerifyOperation;
import com.digitafirma.validador.domain.signature.SignatureValidator;
import com.digitafirma.validador.domain.signature.SignatureVerifyOperation;
import com.digitafirma.validador.domain.xml.XmlValidator;
import com.digitafirma.validador.domain.xml.XmlVerifyOperation;
import com.digitafirma.validador.infra.constancy.Nom151Validator;
import com.digitafirma.validador.infra.pdf.PdfValidatorImpl;
import com.digitafirma.validador.infra.signature.SignatureValidatorImpl;
import com.digitafirma.validador.infra.xml.XmlValidatorImpl;

public class TestValidatorCase {

    protected XmlVerifyOperation xmlVerifyOperation() {
        return new XmlVerifyOperationImpl(xmlValidator());
    }

    protected PdfVerifyOperation pdfVerifyOperation() {
        return new PdfVerifyOperationImpl(pdfValidator());
    }

    protected SignatureVerifyOperation signatureVerifyOperation() {
        return new SignatureVerifyOperationImpl(signatureValidator());
    }

    protected XmlValidator xmlValidator() {

        return new XmlValidatorImpl(
                pdfValidator(),
                stampValidator(),
                signatureValidator()
        );
    }

    protected SignatureValidator signatureValidator() {
        return new SignatureValidatorImpl();
    }

    protected PdfValidator pdfValidator() {
        return  new PdfValidatorImpl(signatureValidator(),stampValidator());
    }

    protected TimeStampValidator stampValidator() {
        return new Nom151Validator(signatureValidator());
    }
}
