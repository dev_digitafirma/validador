/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.xml.adapter;


import com.digitafirma.validador.domain.DigestAlgorithm;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * @author gamboa on 6/6/20
 */
public class XmlAdapterTest {

    private LocalDateAdapter dateTimeAdapter;
    private AlgorithmAdapter algoAdapter;
    private ContentAdapter contentAdapter;

    @Before
    public void setUp() {
        dateTimeAdapter = new LocalDateAdapter();
        algoAdapter = new AlgorithmAdapter();
        contentAdapter = new ContentAdapter();

    }

    @Test
    public void testLocalDateTimeAdapter() throws Exception {
        String strDateTime = "2020-05-30T02:45:05.1748601";

        LocalDateTime dateTime = dateTimeAdapter.unmarshal(strDateTime);

        assertEquals("Year OK", 2020, dateTime.getYear());
        assertEquals("Month OK", 5, dateTime.getMonthValue());
        assertEquals("Day OK", 30, dateTime.getDayOfMonth());

        assertEquals("Hour OK", 2, dateTime.getHour());
        assertEquals("Minute OK", 45, dateTime.getMinute());
        assertEquals("Second OK", 5, dateTime.getSecond());
    }

    @Test
    public void testAlgoAdapter() throws Exception {
        String algo = "SHA256";
        assertEquals(DigestAlgorithm.SHA256, algoAdapter.unmarshal(algo));
    }

    @Test
    public void testContentAdapter() {
        String msg = "RElHSVRBRklSTUE=";
        byte[] is = contentAdapter.unmarshal(msg);
        assertNotNull(is);
    }


}
