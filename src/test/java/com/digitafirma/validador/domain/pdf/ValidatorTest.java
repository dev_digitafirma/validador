/*
 * Copyright (c) 2020    DigitaFirma
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge , publish, distribute, sublicense, and / or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 */

package com.digitafirma.validador.domain.pdf;

import com.digitafirma.validador.domain.DigitalDocument;
import com.digitafirma.validador.domain.NoCertificateException;
import com.digitafirma.validador.domain.NoSignerException;
import com.digitafirma.validador.domain.TestValidatorCase;
import com.digitafirma.validador.domain.constancy.CertStatus;
import com.digitafirma.validador.domain.constancy.InvalidCertCountException;
import com.digitafirma.validador.util.IO;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.tsp.TSPException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

/**
 * @author gamboa on 6/21/20
 */
public class ValidatorTest extends TestValidatorCase {

    @Test
    public void successValidationOnPdfSignatureByPdfBox()
            throws OperatorCreationException,
            GeneralSecurityException,
            IOException,
            NoCertificateException,
            NoSignerException,
            TSPException,
            CMSException, InvalidCertCountException {

        PdfValidator validator = pdfValidator();

        byte[] signedData = Base64.getDecoder()
                .decode(IO.getResourceAsString(getClass().getResourceAsStream("/pdfbox/pdfSignedData.b64")));

        byte[] contents = Base64.getDecoder()
                .decode(IO.getResourceAsString(getClass().getResourceAsStream("/pdfbox/contentEntry.b64")));

        List<PdfSignInfo> signInfos = new ArrayList<>();
        signInfos.add(new PdfSignInfo(Date.from(Instant.now()), contents));

        DigitalDocument pdfSignedData = new DigitalDocument("SignedPDF", signedData);

        PdfVerifyReq req = new PdfVerifyReq(pdfSignedData, signInfos);

        SignedDocumentValidResult result = validator.validate(req);
        Assert.assertTrue(result.getSignerResults().get(0).isSignatureValid());

        CertStatus certStatus = result
                .getSignerResults()
                .get(0)
                .getCertResult()
                .getCertStatus();

        Assert.assertEquals(CertStatus.EXPIRED, certStatus);
        Assert.assertFalse(result.getSignerResults().get(0).getCertResult().isValidOnSignatureDate());

    }



}

