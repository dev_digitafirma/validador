module.exports = {
  publicPath: process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod'
      ? 'https://validador.verificamex.com/'
      : 'http://localhost:8080/',
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ],
  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: 'all'
      }
    }
  }
  // devServer: {
  //   overlay: {
  //     warnings: true,
  //     errors: true
  //   }
  // }
}

