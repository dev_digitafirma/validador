// axios
import axios from 'axios'

const baseURL = process.env.BASE_URL + '/api'

export default axios.create({
  baseURL,
  headers: {
    'Content-Type': 'multipart/form-data'
  }
})
